package cz.dix.alg.genetics;

import cz.dix.alg.Algorithm;
import cz.dix.alg.genetics.strategy.*;

/**
 * Class that represents generic alg algorithm.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class GeneticsAlgorithm implements Algorithm {

    private final Population population;
    private final double pairingProbability;
    private final double mutationProbability;
    private final SelectionStrategy selectionStrategy;
    private final PartnerSelectionStrategy partnerSelectionStrategy;
    private final PairingStrategy pairingStrategy;
    private final ReplacementStrategy replacementStrategy;
    private final RestrictionStrategy restrictionStrategy;
    private final MutationStrategy mutationStrategy;
    private final EndingStrategy endingStrategy;

    private GeneticsAlgorithm(Builder input) {
        this.population = input.population;
        this.pairingProbability = input.pairingProbability;
        this.mutationProbability = input.mutationProbability;
        this.selectionStrategy = input.selectionStrategy;
        this.partnerSelectionStrategy = input.partnerSelectionStrategy;
        this.pairingStrategy = input.pairingStrategy;
        this.replacementStrategy = input.replacementStrategy;
        this.restrictionStrategy = input.restrictionStrategy;
        this.mutationStrategy = input.mutationStrategy;
        this.endingStrategy = input.endingStrategy;
    }

    /**
     * {@inheritDoc}
     */
    public Output solve() {
        long startTime = System.currentTimeMillis();
        Population localPopulation = population;

        while (!endingStrategy.isEnd()) {

            localPopulation = selectionStrategy.makeSelection(localPopulation);
            for (int idx = 0; idx < localPopulation.getSize(); idx++) {
                PopulationMember member = localPopulation.getMember(idx);

                if (Math.random() < pairingProbability) {
                    PopulationMember pairingPartner = partnerSelectionStrategy.findPairingPartner(localPopulation, member);

                    PopulationMember[] children = pairingStrategy.pair(localPopulation, member, pairingPartner);
                    PopulationMember child1 = children[0];
                    PopulationMember child2 = children[1];

                    if (!localPopulation.satisfies(child1)) {
                        child1 = restrictionStrategy.restrict(localPopulation, member, child1);
                    }
                    if (!localPopulation.satisfies(child2)) {
                        child2 = restrictionStrategy.restrict(localPopulation, pairingPartner, child2);
                    }

                    replacementStrategy.replace(localPopulation, member, child1);
                    replacementStrategy.replace(localPopulation, pairingPartner, child2);
                }

                if (Math.random() < mutationProbability) {
                    PopulationMember mutatedMember = mutationStrategy.mutate(member);
                    if (!localPopulation.satisfies(mutatedMember)) {
                        mutatedMember = restrictionStrategy.restrict(localPopulation, member, mutatedMember);
                    }
                    localPopulation.replaceMember(member, mutatedMember);
                }
            }

            endingStrategy.update(localPopulation);
        }

        PopulationMember bestMember = localPopulation.getBestMember();
        if (bestMember == null) {
            return null;
        }
        int fitness = localPopulation.getFitness(bestMember);
        long elapsed = System.currentTimeMillis() - startTime;
        return new Output(bestMember.getParts(), fitness, elapsed);

    }

    /**
     * KnapsackInput for the alg algorithm that contains all required strategies
     */
    public static class Builder {

        private Population population;
        private double pairingProbability;
        private double mutationProbability;
        private SelectionStrategy selectionStrategy;
        private PartnerSelectionStrategy partnerSelectionStrategy;
        private PairingStrategy pairingStrategy;
        private ReplacementStrategy replacementStrategy;
        private RestrictionStrategy restrictionStrategy;
        private MutationStrategy mutationStrategy;
        private EndingStrategy endingStrategy;

        public Builder population(Population population) {
            this.population = population;
            return this;
        }

        public Builder pairingProbability(double pairingProbability) {
            this.pairingProbability = pairingProbability;
            return this;
        }

        public Builder mutationProbability(double mutationProbability) {
            this.mutationProbability = mutationProbability;
            return this;
        }

        public Builder selectionStrategy(SelectionStrategy selectionStrategy) {
            this.selectionStrategy = selectionStrategy;
            return this;
        }

        public Builder partnerSelectionStrategy(PartnerSelectionStrategy partnerSelectionStrategy) {
            this.partnerSelectionStrategy = partnerSelectionStrategy;
            return this;
        }

        public Builder pairingStrategy(PairingStrategy pairingStrategy) {
            this.pairingStrategy = pairingStrategy;
            return this;
        }

        public Builder replacementStrategy(ReplacementStrategy replacementStrategy) {
            this.replacementStrategy = replacementStrategy;
            return this;
        }

        public Builder restrictionStrategy(RestrictionStrategy restrictionStrategy) {
            this.restrictionStrategy = restrictionStrategy;
            return this;
        }

        public Builder mutationStrategy(MutationStrategy mutationStrategy) {
            this.mutationStrategy = mutationStrategy;
            return this;
        }

        public Builder endingStrategy(EndingStrategy endingStrategy) {
            this.endingStrategy = endingStrategy;
            return this;
        }

        public GeneticsAlgorithm build() {
            return new GeneticsAlgorithm(this);
        }
    }
}
