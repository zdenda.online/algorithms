package cz.dix.alg.genetics;

/**
 * A population member represents a single instance of the given problem. Every state can be represented as a boolean
 * array and thus the population member is wrapper over this array which can also contain the penalization which is
 * used in the genetic algorithms.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class PopulationMember {

    private int penalization = 0;
    protected boolean[] parts;

    /**
     * Creates a new population member from the given parts.
     *
     * @param parts initial parts
     */
    public PopulationMember(boolean[] parts) {
        this.parts = parts;
    }

    /**
     * Mutates the specified part of the member. It simply switches boolean flag at given flag.
     *
     * @param idx index of switched flag
     */
    public void mutatePart(int idx) {
        this.parts[idx] = !this.parts[idx];
    }

    /**
     * Gets parts of the member.
     *
     * @return parts of the member
     */
    public boolean[] getParts() {
        return this.parts;
    }

    /**
     * Gets actual penalization of the member.
     *
     * @return actual penalization
     */
    public int getPenalization() {
        return this.penalization;
    }

    /**
     * Sets the penalization of the member.
     *
     * @param penalization penalization to be set
     */
    public void setPenalization(int penalization) {
        this.penalization = penalization;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (boolean part : parts) {
            sb.append((part) ? "1" : "0");
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Creates a deep copy of the member.
     *
     * @return new member with same contents as the previous member
     */
    public PopulationMember deepCopy() {
        boolean[] newParts = new boolean[parts.length];
        System.arraycopy(parts, 0, newParts, 0, parts.length);
        PopulationMember copiedMember = new PopulationMember(newParts);
        copiedMember.setPenalization(penalization);
        return copiedMember;
    }

}
