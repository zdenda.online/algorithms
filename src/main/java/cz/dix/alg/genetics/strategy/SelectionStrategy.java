package cz.dix.alg.genetics.strategy;

import cz.dix.alg.genetics.Population;

/**
 * Selection strategy represents a way how should be performed selection over whole population in a alg algorithm.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public interface SelectionStrategy {

    /**
     * Makes a selection over the population.
     *
     * @param population population to be processed
     * @return population after the selection
     */
    Population makeSelection(Population population);
}
