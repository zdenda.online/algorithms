package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;
import cz.dix.alg.genetics.strategy.SelectionStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * Roulette selection strategy. (see google for more)
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class RouletteSelectionStrategy implements SelectionStrategy {

    private Map<Integer, Integer> indexMap;
    private int maxScale;

    public RouletteSelectionStrategy() {
        this.indexMap = new HashMap<Integer, Integer>();
    }

    /**
     * {@inheritDoc}
     */
    public Population makeSelection(Population originalPopulation) {
        int desiredSize = originalPopulation.getSize();
        initMap(originalPopulation);

        Population newPopulation = originalPopulation.emptyClone();
        int actualSize = 0;

        while (actualSize != desiredSize) {
            PopulationMember newMember = doRoulette(originalPopulation);
            newPopulation.addMember(newMember.deepCopy());
            actualSize++;
        }
        return newPopulation;
    }

    /**
     * Initializes the map for the roulette.
     *
     * @param originalPopulation original population
     */
    private void initMap(Population originalPopulation) {
        int scaleCnt = 0;
        for (int memberIdx = 0; memberIdx < originalPopulation.getSize(); memberIdx++) {
            PopulationMember member = originalPopulation.getMember(memberIdx);
            int fitness = originalPopulation.getFitness(member);
            for (int j = 0; j < fitness; j++) {
                this.indexMap.put(scaleCnt++, memberIdx);
            }
        }
        this.maxScale = scaleCnt - 1;
    }

    /**
     * Makes one roulette selection over the population.
     *
     * @param originalPopulation original population
     * @return selected member
     */
    private PopulationMember doRoulette(Population originalPopulation) {
        int randomRouletteNumber = (int) (Math.random() * this.maxScale);
        int memberIdx = this.indexMap.get(randomRouletteNumber);
        return originalPopulation.getMember(memberIdx);
    }
}
