package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.strategy.PairingStrategy;
import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Pairing strategy that takes one random point in the array of members and takes left part of the member and right
 * part of the partner and combines them into new children.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class OnePointPairingStrategy implements PairingStrategy {

    /**
     * {@inheritDoc}
     */
    public PopulationMember[] pair(Population population, PopulationMember member, PopulationMember partner) {
        boolean[] parts1 = member.getParts();
        boolean[] parts2 = partner.getParts();
        int partsSize = parts1.length;
        boolean[] child1Parts = new boolean[partsSize];
        boolean[] child2Parts = new boolean[partsSize];

        int randomPoint = (int) (Math.random() * partsSize);
        for (int i = 0; i < partsSize; i++) {
            child1Parts[i] = (randomPoint > i) ? parts1[i] : parts2[i];
            child2Parts[i] = (randomPoint > i) ? parts2[i] : parts1[i];
        }

        PopulationMember child1 = population.createMember(child1Parts);
        PopulationMember child2 = population.createMember(child2Parts);
        return new PopulationMember[]{child1, child2};
    }
}
