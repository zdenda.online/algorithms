package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.strategy.PairingStrategy;
import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Pairing strategy where first child takes information from mother and second from the father. Who takes which part
 * is chosen randomly (50% that first child takes mother's information).
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class UniformPairingStrategy implements PairingStrategy {

    /**
     * {@inheritDoc}
     */
    public PopulationMember[] pair(Population population, PopulationMember member, PopulationMember partner) {
        boolean[] parts1 = member.getParts();
        boolean[] parts2 = partner.getParts();
        int partsSize = parts1.length;
        boolean[] child1Parts = new boolean[partsSize];
        boolean[] child2Parts = new boolean[partsSize];

        boolean takeFromDaddy = Math.random() > 0.5;
        for (int i = 0; i < partsSize; i++) {
            child1Parts[i] = takeFromDaddy ? parts1[i] : parts2[i];
            child2Parts[i] = takeFromDaddy ? parts2[i] : parts1[i];
        }

        PopulationMember child1 = population.createMember(child1Parts);
        PopulationMember child2 = population.createMember(child2Parts);
        return new PopulationMember[]{child1, child2};
    }
}
