package cz.dix.alg.genetics.strategy;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Class represents the strategy of selecting pairing partner for the member.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public interface PartnerSelectionStrategy {

    /**
     * Finds a pairing partner for the given member with the population.
     *
     * @param population population to be searched for the partner
     * @param member     member to be used for the search
     * @return pairing partner for the member
     */
    PopulationMember findPairingPartner(Population population, PopulationMember member);
}
