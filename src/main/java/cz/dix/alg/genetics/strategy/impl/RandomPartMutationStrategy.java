package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.strategy.MutationStrategy;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Simple mutation strategy that mutates one random bit within the given member.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class RandomPartMutationStrategy implements MutationStrategy {

    public PopulationMember mutate(PopulationMember originalMember) {
        PopulationMember mutatedMember = originalMember.deepCopy();
        int partsSize = originalMember.getParts().length;
        int mutatePartIdx = (int) (Math.random() * partsSize);
        mutatedMember.mutatePart(mutatePartIdx);
        return mutatedMember;
    }
}
