package cz.dix.alg.genetics.strategy;

import cz.dix.alg.genetics.PopulationMember;

/**
 * Mutation strategy represents the way how the mutation of the population member is performed.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public interface MutationStrategy {

    /**
     * Mutates given population member and returns a new one as a replacement.
     *
     * @param member member to be mutated
     * @return mutated member
     */
    PopulationMember mutate(PopulationMember member);

}
