package cz.dix.alg.genetics.strategy;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Replacement strategy represents the way how the new members should replace the old members.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public interface ReplacementStrategy {

    /**
     * Replaces an old member by a new member in the specified population.
     *
     * @param population population of members
     * @param oldMember  old member to be replaced
     * @param newMember  new member for the replace
     */
    void replace(Population population, PopulationMember oldMember, PopulationMember newMember);
}
