package cz.dix.alg.genetics.strategy;

import cz.dix.alg.genetics.Population;

/**
 * Ending strategy represents condition when the alg algorithm should be ended.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public interface EndingStrategy {

    /**
     * Gets a flag whether ending condition for the algorithm has been met.
     *
     * @return true if algorithm should end, otherwise false
     */
    boolean isEnd();

    /**
     * Updates the strategy and passes it actual population state.
     *
     * @param population actual population
     */
    void update(Population population);

}
