package cz.dix.alg.genetics.strategy;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Restriction strategy represents how should be restricted members if they not satisfy the population.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public interface RestrictionStrategy {

    /**
     * Restricts a new child within the population.
     *
     * @param population population of the members
     * @param ancestor   ancestor of the child
     * @param child      new child to be restricted
     * @return new population member (typically a child)
     */
    PopulationMember restrict(Population population, PopulationMember ancestor, PopulationMember child);

}
