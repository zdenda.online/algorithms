package cz.dix.alg.genetics.strategy;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Pairing strategy represents how should be pairing of two given members made.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public interface PairingStrategy {

    /**
     * Makes a pairing of two given members.
     *
     * @param population population of the members
     * @param member     member to be paired
     * @param partner    partner of the member to be paired
     * @return two new population members (children)
     */
    PopulationMember[] pair(Population population, PopulationMember member, PopulationMember partner);

}
