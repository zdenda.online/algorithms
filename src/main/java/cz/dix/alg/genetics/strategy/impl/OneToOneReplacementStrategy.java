package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;
import cz.dix.alg.genetics.strategy.ReplacementStrategy;

/**
 * Simple replacement strategy that replaces the old member by the new member (1:1).
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class OneToOneReplacementStrategy implements ReplacementStrategy {

    /**
     * {@inheritDoc}
     */
    public void replace(Population population, PopulationMember oldMember, PopulationMember newMember) {
        population.replaceMember(oldMember, newMember);
    }
}
