package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;
import cz.dix.alg.genetics.strategy.SelectionStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * Stochastic selection strategy (see google for more)
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class StochasticSelectionStrategy implements SelectionStrategy {

    private Map<Integer, Integer> indexMap;
    private int maxScale;
    private int actualPosition;
    private int jump;

    /**
     * Creates a new instance of the strategy.
     *
     * @param jump size of one jump in the population
     */
    public StochasticSelectionStrategy(int jump) {
        this.indexMap = new HashMap<Integer, Integer>();
        this.jump = jump;
    }

    /**
     * {@inheritDoc}
     */
    public Population makeSelection(Population originalPopulation) {
        int desiredSize = originalPopulation.getSize();
        initMap(originalPopulation);

        Population newPopulation = originalPopulation.emptyClone();
        int actualSize = 0;

        while (actualSize != desiredSize) {
            PopulationMember newMember = doJump(originalPopulation);
            newPopulation.addMember(newMember.deepCopy());
            actualSize++;
        }
        return newPopulation;
    }

    /**
     * Initializes the map for the selection.
     *
     * @param originalPopulation original population
     */
    private void initMap(Population originalPopulation) {
        int scaleCnt = 0;
        for (int memberIdx = 0; memberIdx < originalPopulation.getSize(); memberIdx++) {
            PopulationMember member = originalPopulation.getMember(memberIdx);
            int fitness = originalPopulation.getFitness(member);
            for (int j = 0; j < fitness; j++) {
                this.indexMap.put(scaleCnt++, memberIdx);
            }
        }
        this.maxScale = scaleCnt - 1;
        this.actualPosition = (int) (Math.random() * this.maxScale);
    }

    /**
     * Makes a jump in the population.
     *
     * @param originalPopulation original population
     * @return member at the next jump
     */
    private PopulationMember doJump(Population originalPopulation) {
        this.actualPosition = (actualPosition + jump) % maxScale;
        int memberIdx = this.indexMap.get(actualPosition);
        return originalPopulation.getMember(memberIdx);
    }
}
