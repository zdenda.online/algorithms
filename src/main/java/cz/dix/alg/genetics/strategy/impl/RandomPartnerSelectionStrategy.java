package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.strategy.PartnerSelectionStrategy;
import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Selection strategy that finds a random partner in the population (excluding the member itself) for the pairing.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class RandomPartnerSelectionStrategy implements PartnerSelectionStrategy {

    /**
     * {@inheritDoc}
     */
    public PopulationMember findPairingPartner(Population population, PopulationMember member) {
        int populationSize = population.getSize();
        PopulationMember partner = null;
        do {
            int partnerIdx = (int) (Math.random() * populationSize);
            partner = population.getMember(partnerIdx);
        } while (member.equals(partner));
        return partner;
    }
}
