package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;
import cz.dix.alg.genetics.strategy.ReplacementStrategy;

/**
 * Replacement strategy that replaces the worst member of the population with new one.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class WorstReplacementStrategy implements ReplacementStrategy {

    /**
     * {@inheritDoc}
     */
    public void replace(Population population, PopulationMember oldMember, PopulationMember newMember) {
        PopulationMember worstMember = population.getWorstMember();
        if (population.satisfies(worstMember) && !population.satisfies(newMember)) {
            return; // do not replace by member who does not satisfy
        }
        if (population.getFitness(worstMember) >= population.getFitness(newMember)) {
            return; // do not replace by member with even worse fitness
        }
        population.replaceMember(worstMember, newMember);
    }
}
