package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.strategy.PartnerSelectionStrategy;
import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Simple selection strategy that picks the best member in the population as the pairing partner.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class BestPartnerSelectionStrategy implements PartnerSelectionStrategy {

    /**
     * {@inheritDoc}
     */
    public PopulationMember findPairingPartner(Population population, PopulationMember member) {
        return population.getBestMember();
    }
}
