package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;
import cz.dix.alg.genetics.strategy.RestrictionStrategy;

/**
 * Restriction strategy that kills the newly born child and keeps its ancestor.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class DeathRestrictionStrategy implements RestrictionStrategy {

    public PopulationMember restrict(Population population, PopulationMember ancestor, PopulationMember child) {
        return ancestor; // we kill child and remain its ancestor
    }
}
