package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.strategy.EndingStrategy;
import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Ending strategy that counts number of generation updates and ends when the given number of generations has been
 * processed.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class GenerationsCountEndingStrategy implements EndingStrategy {

    private int generationsCount;
    private int actualGeneration = 1;

    /**
     * Creates a new ending strategy.
     *
     * @param generationsCount number of generations to be processed
     */
    public GenerationsCountEndingStrategy(int generationsCount) {
        this.generationsCount = generationsCount;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isEnd() {
        return (actualGeneration > generationsCount);
    }

    /**
     * {@inheritDoc}
     */
    public void update(Population population) {
        PopulationMember bestMember = population.getBestMember();
//        System.out.println("Finished " + actualGeneration + ". generation, best member is " + bestMember +
//                ", Fitness: " + ((bestMember != null) ? population.getFitness(bestMember) : "<no_fitness>"));
        actualGeneration++;
    }
}
