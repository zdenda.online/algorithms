package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;
import cz.dix.alg.genetics.strategy.SelectionStrategy;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Tournament selection strategy (see google for more)
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class TournamentSelectionStrategy implements SelectionStrategy {

    private int tournamentSize;

    /**
     * Creates a new selection strategy.
     *
     * @param tournamentSize size of the tournament
     */
    public TournamentSelectionStrategy(int tournamentSize) {
        this.tournamentSize = tournamentSize;
    }

    /**
     * {@inheritDoc}
     */
    public Population makeSelection(Population originalPopulation) {
        int desiredSize = originalPopulation.getSize();

        Population newPopulation = originalPopulation.emptyClone();
        int actualSize = 0;

        while (actualSize != desiredSize) {
            PopulationMember newMember = doTournament(originalPopulation);
            newPopulation.addMember(newMember.deepCopy());
            actualSize++;
        }
        return newPopulation;
    }

    /**
     * Makes a tournament for the given tournament size
     *
     * @param originalPopulation original population
     * @return winner of the tournament
     */
    private PopulationMember doTournament(Population originalPopulation) {
        int populationSize = originalPopulation.getSize();
        Collection<PopulationMember> tournamentPlayers = new ArrayList<PopulationMember>();

        while (this.tournamentSize != tournamentPlayers.size()) {
            int randomMemberIdx = (int) (Math.random() * populationSize);
            PopulationMember randomMember = originalPopulation.getMember(randomMemberIdx);
            if (!tournamentPlayers.contains(randomMember)) {
                tournamentPlayers.add(randomMember);
            }
        }

        PopulationMember winner = null;
        int bestFitness = -1;
        for (PopulationMember player : tournamentPlayers) {
            int fitness = originalPopulation.getFitness(player);
            if (fitness > bestFitness) {
                if (winner == null || originalPopulation.satisfies(player)) {
                    winner = player;
                    bestFitness = fitness;
                }
            }
        }
        return winner;
    }
}
