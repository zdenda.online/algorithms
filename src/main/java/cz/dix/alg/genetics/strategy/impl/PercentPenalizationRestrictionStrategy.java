package cz.dix.alg.genetics.strategy.impl;

import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;
import cz.dix.alg.genetics.strategy.RestrictionStrategy;

/**
 * Restriction strategy that penalizes a child by its percents of completion.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class PercentPenalizationRestrictionStrategy implements RestrictionStrategy {

    /**
     * {@inheritDoc}
     */
    public PopulationMember restrict(Population population, PopulationMember ancestor, PopulationMember child) {
        double percentCompletion = population.getPercentCompletion(child);
        int fitness = population.getFitness(child);
        int penalization = (int) ((1 - percentCompletion) * fitness); // penalization measured from percent completion
        child.setPenalization(3 * penalization); // triple the penalization
        return child;
    }
}
