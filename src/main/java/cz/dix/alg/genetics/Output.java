package cz.dix.alg.genetics;

/**
 * Simple java bean which is used for outputs.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class Output {

    private boolean[] resultItemsArray;
    private int fitness;
    private long elapsedTime;

    public Output(boolean[] resultItemsArray, int fitness, long elapsedTime) {
        this.resultItemsArray = resultItemsArray;
        this.fitness = fitness;
        this.elapsedTime = elapsedTime;
    }

    /**
     * Gets the results.
     *
     * @return results
     */
    public boolean[] getResultItemsArray() {
        return resultItemsArray;
    }

    /**
     * Gets the fitness of the best member.
     *
     * @return fitness of best member
     */
    public int getFitness() {
        return fitness;
    }

    /**
     * Gets elapsed time during processing.
     *
     * @return elapsed time
     */
    public long getElapsedTime() {
        return elapsedTime;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(fitness);
        sb.append(" [").append(resultArrayToString()).append("]");
        return sb.toString();
    }

    /**
     * Transforms a result to the simple {@link String}.
     *
     * @return result as string
     */
    private String resultArrayToString() {
        StringBuilder sb = new StringBuilder();
        for (boolean val : resultItemsArray) {
            sb.append(val ? "1" : "0");
        }
        return sb.toString();
    }
}
