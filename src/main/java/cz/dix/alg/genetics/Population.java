package cz.dix.alg.genetics;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class that represents one population of alg algorithm. Every implementation of the population must
 * implement required methods which may differ for every specific problem.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public abstract class Population {

    protected List<PopulationMember> members = new ArrayList<PopulationMember>();

    /**
     * Creates a new member of the population from given parts.
     *
     * @param parts parts of the new member
     * @return
     */
    public abstract PopulationMember createMember(boolean[] parts);

    /**
     * Creates a clone of the population without its members.
     *
     * @return clone of the population
     */
    public abstract Population emptyClone();

    /**
     * Gets a fitness of the population member (how good is the member).
     *
     * @param member member to be checked
     * @return fitness of given member
     */
    public abstract int getFitness(PopulationMember member);

    /**
     * Gets a flag whether the member satisfies (is possible solution of) the problem.
     *
     * @param member member to be checked
     * @return true whether member is the solution
     */
    public abstract boolean satisfies(PopulationMember member);

    /**
     * Gets the value from how many percents is the member solution of the problem. Typically used in
     * {@link cz.dix.alg.genetics.strategy.RestrictionStrategy} strategies.
     *
     * @param member member to be checked
     * @return value in interval <0,1> from how many percents is the member complete
     */
    public abstract double getPercentCompletion(PopulationMember member);

    /**
     * Gets the best member of the population measured by the fitness (member must also satisfy as the solution).
     *
     * @return best member of the population
     */
    public PopulationMember getBestMember() {
        PopulationMember bestMember = null;
        int bestFitness = -1;
        for (PopulationMember member : members) {
            int fitness = getFitness(member);
            if (fitness > bestFitness && satisfies(member)) {
                bestMember = member;
                bestFitness = fitness;
            }
        }
        return bestMember;
    }

    /**
     * Gets the worst member of the population measured by the fitness (satisfaction to the solution is not checked)
     *
     * @return worst member of the population
     */
    public PopulationMember getWorstMember() {
        PopulationMember worstMember = null;
        int worstFitness = Integer.MAX_VALUE;
        for (PopulationMember member : members) {
            int fitness = getFitness(member);
            if (fitness < worstFitness) {
                worstMember = member;
                worstFitness = fitness;
            }
        }
        return worstMember;
    }

    /**
     * Gets the member of population on given index.
     *
     * @param idx index of the member
     * @return member at given index
     */
    public PopulationMember getMember(int idx) {
        return this.members.get(idx);
    }

    /**
     * Adds a new member to the population.
     *
     * @param member member to be added
     */
    public void addMember(PopulationMember member) {
        this.members.add(member);
    }

    /**
     * Gets size of the population.
     *
     * @return size of the population
     */
    public int getSize() {
        return this.members.size();
    }

    /**
     * Replaces specified member by a new member.
     *
     * @param oldMember member to be replaced
     * @param newMember new member that takes position of old member
     */
    public void replaceMember(PopulationMember oldMember, PopulationMember newMember) {
        int oldMemberIdx = this.members.indexOf(oldMember);
        if (oldMemberIdx == -1) {
            return;
        }
        this.members.remove(oldMemberIdx);
        this.members.add(oldMemberIdx, newMember);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int cnt = 1;
        sb.append("-------\n");
        for (PopulationMember member : members) {
            sb.append(cnt++).append(".: ").append(member).append("\n");
        }
        sb.append("--------");
        return sb.toString();
    }

}
