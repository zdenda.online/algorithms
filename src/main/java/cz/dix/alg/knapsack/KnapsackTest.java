package cz.dix.alg.knapsack;

import cz.dix.alg.genetics.GeneticsAlgorithm;
import cz.dix.alg.genetics.Output;
import cz.dix.alg.genetics.strategy.impl.*;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Tests the knapsack algorithms.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class KnapsackTest {

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#0.000");

    public static void main(String[] args) {
        int runs = 10;

        int itemsCnt = 50;
        int instancesCnt = 1;
        int maxWeight = 50;
        int maxValue = 100;
        double weightsToCapacityRatio = 0.4;
        int granularityFactor = 0;
        InstancesGenerator instancesGenerator = new InstancesGenerator(itemsCnt, instancesCnt, maxWeight, maxValue,
                weightsToCapacityRatio, granularityFactor);
        List<KnapsackInput> inputs = instancesGenerator.generateInputs();
        KnapsackInput input = inputs.get(0);

        int populationSize = 500;
        int tournamentSize = 3;
        int generationsCnt = 10;
        double pairingProbability = 0.7;
        double mutationProbability = 0.3;

        double relErrorsSum = 0;
        double lowestSecs = Double.MAX_VALUE;
        int finishedRuns = 0;
        for (int run = 0; run < runs; run++) {

            GeneticsAlgorithm algorithm = new GeneticsAlgorithm.Builder()
                    .pairingProbability(pairingProbability)
                    .mutationProbability(mutationProbability)
                    .selectionStrategy(new TournamentSelectionStrategy(tournamentSize))
                    .partnerSelectionStrategy(new RandomPartnerSelectionStrategy())
                    .pairingStrategy(new OnePointPairingStrategy())
                    .replacementStrategy(new WorstReplacementStrategy())
                    .restrictionStrategy(new DeathRestrictionStrategy())
                    .mutationStrategy(new RandomPartMutationStrategy())
                    .endingStrategy(new GenerationsCountEndingStrategy(generationsCnt))
                    .population(new KnapsackPopulation(input, populationSize))
                    .build();

            Output output = algorithm.solve();
            if (output != null) {
                double secs = ((double) output.getElapsedTime()) / 1000;
                if (secs < lowestSecs) {
                    lowestSecs = secs;
                }

                KnapsackDynaProgAlgorithm dpAlg = new KnapsackDynaProgAlgorithm(input);
                Output optimum = dpAlg.solve();

                System.out.println("Gen: " + output);
                System.out.println("Opt: " + optimum);
                System.out.println("-----------------------------------");

                double relError = ((double) (optimum.getFitness() - output.getFitness())) / optimum.getFitness();
                relErrorsSum += relError;
                finishedRuns++;
            }

        }

        double relErrorAvg = relErrorsSum / finishedRuns;
        System.out.println("Runs: (finished/total): " + finishedRuns + "/" + runs);
        System.out.println("Average relative error of finished runs: " + DECIMAL_FORMAT.format(relErrorAvg * 100) + "%");
        System.out.println("Lowest time of finished runs: " + DECIMAL_FORMAT.format(lowestSecs) + "s");
    }
}
