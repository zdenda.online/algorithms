package cz.dix.alg.knapsack;

import cz.dix.alg.Algorithm;
import cz.dix.alg.genetics.Output;

/**
 * Algorithm that solves a knapsack problem by the dynamic programming method.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class KnapsackDynaProgAlgorithm implements Algorithm {

    private TableCell[][] table;
    private int[] weights;
    private int[] values;
    private int bagSize;

    public KnapsackDynaProgAlgorithm(KnapsackInput input) {
        this.weights = input.getWeights();
        this.values = input.getValues();
        this.bagSize = input.getBagSize();
    }

    /**
     * {@inheritDoc}
     */
    public Output solve() {
        long startTime = System.currentTimeMillis();
        int itemsCount = weights.length;

        initTable(bagSize, itemsCount);
        for (int i = 1; i < bagSize + 1; i++) {
            for (int j = 1; j < itemsCount + 1; j++) {
                countCell(i, j);
            }
        }

        TableCell finalCell = table[bagSize][itemsCount];
        int finalValue = finalCell.getValue();
        boolean[] result = reconstructResult(weights.length, finalCell);
        long elapsed = System.currentTimeMillis() - startTime;
        return new Output(result, finalValue, elapsed);
    }

    /**
     * Reconstructs variant from the table.
     *
     * @param size      size of result array
     * @param finalCell top right cell (final cell with result) from which backtracking should start
     * @return reconstructed result
     */
    private boolean[] reconstructResult(int size, TableCell finalCell) {
        boolean[] result = new boolean[size];
        TableCell actual = finalCell;
        for (int i = size - 1; i >= 0; i--) {
            TableCell ancestor = actual.getAncestor();
            result[i] = actual.getWeight() != ancestor.getWeight();
            actual = ancestor;
        }
        return result;
    }

    /**
     * Initializes table.
     *
     * @param bagSize    size of bag
     * @param itemsCount count of items
     */
    private void initTable(int bagSize, int itemsCount) {
        table = new TableCell[bagSize + 1][itemsCount + 1];

        TableCell ancestor = null;
        for (int i = 0; i < bagSize + 1; i++) {
            table[i][0] = new TableCell(ancestor);
            ancestor = table[i][0];
        }

        ancestor = null;
        for (int i = 0; i < itemsCount + 1; i++) {
            table[0][i] = new TableCell(ancestor);
            ancestor = table[0][i];
        }
    }

    /**
     * Counts value of cell on specified coordinates.
     *
     * @param rowIdx index of row
     * @param colIdx index of column
     */
    private void countCell(int rowIdx, int colIdx) {
        TableCell notAdded = table[rowIdx][colIdx - 1];

        int weight = weights[colIdx - 1];
        int value = values[colIdx - 1];
        int addedRowIdx = rowIdx - weight;

        TableCell added;
        if (addedRowIdx < 0) {
            added = new TableCell();
        } else {
            TableCell ancestor = table[addedRowIdx][colIdx - 1];
            int ancestorWeight = ancestor.getWeight();
            int ancestorValue = ancestor.getValue();
            added = new TableCell(ancestorWeight + weight, ancestorValue + value, ancestor);
        }

        TableCell newCell;
        if (added.isBetter(notAdded)) {
            newCell = added;
        } else {
            newCell = new TableCell(notAdded);
        }
        table[rowIdx][colIdx] = newCell;
    }

    /**
     * Cell of the table.
     */
    private class TableCell {

        private int weight;
        private int value;
        private TableCell ancestor;

        public TableCell() {
            this.weight = 0;
            this.value = 0;
        }

        public TableCell(TableCell ancestor) {
            if (ancestor != null) {
                this.weight = ancestor.getWeight();
                this.value = ancestor.getValue();
            }
            this.ancestor = ancestor;
        }

        public TableCell(int weight, int value, TableCell ancestor) {
            this.weight = weight;
            this.value = value;
            this.ancestor = ancestor;
        }

        public TableCell getAncestor() {
            return ancestor;
        }

        public int getValue() {
            return value;
        }

        public int getWeight() {
            return weight;
        }

        public boolean isBetter(TableCell other) {
            return value > other.getValue();
        }
    }
}
