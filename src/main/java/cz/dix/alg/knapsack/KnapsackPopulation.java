package cz.dix.alg.knapsack;

import cz.dix.alg.Utils;
import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Implementation of the population for the knapsack problem.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class KnapsackPopulation extends Population {

    private int[] weights;
    private int[] values;
    private int bagSize;

    /**
     * Creates a new knapsack population.
     *
     * @param input input of knapsack problem
     * @param size  size of the population
     */
    public KnapsackPopulation(KnapsackInput input, int size) {
        this.weights = input.getWeights();
        this.values = input.getValues();
        this.bagSize = input.getBagSize();

        for (int i = 0; i < size; i++) {
            boolean[] parts;
            do { // we don't want cripples in start population
                parts = Utils.genRandomBoolArray(weights.length);
            } while (multiplicate(parts, weights) > bagSize);

            PopulationMember member = new PopulationMember(parts);
            members.add(member);
        }
    }

    /**
     * for internal use only
     *
     * @see #emptyClone()
     */
    private KnapsackPopulation(int[] weights, int[] values, int bagSize) {
        this.weights = weights;
        this.values = values;
        this.bagSize = bagSize;
    }


    /**
     * {@inheritDoc}
     */
    public PopulationMember createMember(boolean[] parts) {
        return new PopulationMember(parts);
    }

    /**
     * {@inheritDoc}
     */
    public Population emptyClone() {
        return new KnapsackPopulation(weights, values, bagSize);
    }

    /**
     * {@inheritDoc}
     */
    public int getFitness(PopulationMember member) {
        return multiplicate(member.getParts(), this.values) - member.getPenalization();
    }

    /**
     * {@inheritDoc}
     */
    public boolean satisfies(PopulationMember member) {
        int weightsSum = multiplicate(member.getParts(), this.weights);
        return weightsSum <= bagSize;
    }

    /**
     * {@inheritDoc}
     */
    public double getPercentCompletion(PopulationMember member) {
        throw new RuntimeException("This method is not supported for the knapsack problem algorithm.");
    }

    private int multiplicate(boolean[] bitVector, int[] muls) {
        int sum = 0;
        for (int i = 0; i < bitVector.length; i++) {
            if (bitVector[i]) {
                sum += muls[i];
            }
        }
        return sum;
    }

}
