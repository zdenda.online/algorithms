package cz.dix.alg.knapsack;

/**
 * Class representing input of the knapsack problem.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class KnapsackInput {

    private final int[] weights;
    private final int[] values;
    private final int bagSize;

    /**
     * Creates a new input for knapsack problem.
     *
     * @param weights weights of the items
     * @param values  values of the items
     * @param bagSize size of the bag
     */
    public KnapsackInput(int[] weights, int[] values, int bagSize) {
        this.weights = weights;
        this.values = values;
        this.bagSize = bagSize;
    }

    /**
     * Gets a size of the bag.
     *
     * @return size of bag
     */
    public int getBagSize() {
        return bagSize;
    }

    /**
     * Gets values of the items.
     *
     * @return values of the items
     */
    public int[] getValues() {
        return values;
    }

    /**
     * Gets weights of the items.
     *
     * @return weights of the items
     */
    public int[] getWeights() {
        return weights;
    }
}
