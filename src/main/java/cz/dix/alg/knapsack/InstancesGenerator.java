package cz.dix.alg.knapsack;

import java.util.ArrayList;
import java.util.List;

/**
 * Generator of knapsack problem inputs.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class InstancesGenerator {

    private int itemsCnt;
    private int instancesCnt;
    private int maxWeight;
    private int maxValue;
    private double bagCapacityToItemsSumRatio;
    private int granularityFactor;

    public InstancesGenerator(int itemsCnt, int instancesCnt, int maxWeight, int maxValue,
                              double bagCapacityToItemsSumRatio, int granularityFactor) {
        this.itemsCnt = itemsCnt;
        this.instancesCnt = instancesCnt;
        this.maxWeight = maxWeight;
        this.maxValue = maxValue;
        this.bagCapacityToItemsSumRatio = bagCapacityToItemsSumRatio;
        this.granularityFactor = granularityFactor;
    }

    public List<KnapsackInput> generateInputs() {
        List<KnapsackInput> out = new ArrayList<KnapsackInput>();

        for (int i = 0; i < instancesCnt; i++) {

            int weights[] = new int[itemsCnt];
            int values[] = new int[itemsCnt];
            int weightsSum = 0;
            for (int j = 0; j < itemsCnt; j++) {

                int weight = 0;
                do {
                    weight = generateValue(maxWeight);
                } while (weight < (maxWeight - granularityFactor));

                weights[j] = weight;
                weightsSum += weight;
                values[j] = generateValue(maxValue);
            }
            int bagSize = (int) (weightsSum * bagCapacityToItemsSumRatio);

            KnapsackInput input = new KnapsackInput(weights, values, bagSize);
            out.add(input);
        }

        return out;
    }

    /**
     * Generates a random value.
     *
     * @param max maximal limit
     * @return random value
     */
    private static int generateValue(int max) {
        return (int) (Math.random() * max) + 1;
    }

}
