package cz.dix.alg;

import cz.dix.alg.genetics.Output;

/**
 * Unified interface for all the algorithms
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public interface Algorithm {

    /**
     * Solves the given implementation of the algorithm.
     */
    Output solve();
}
