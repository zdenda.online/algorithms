package cz.dix.alg;

/**
 * Utilities for the algorithms.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class Utils {

    /**
     * Generates a random boolean array of given size.
     *
     * @param size size of array to be generated
     * @return random boolean array
     */
    public static boolean[] genRandomBoolArray(int size) {
        boolean[] out = new boolean[size];
        for (int i = 0; i < out.length; i++) {
            out[i] = (Math.random() > 0.5);
        }
        return out;
    }

}
