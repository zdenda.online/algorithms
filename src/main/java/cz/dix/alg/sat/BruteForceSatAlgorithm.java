package cz.dix.alg.sat;

import cz.dix.alg.Algorithm;
import cz.dix.alg.genetics.Output;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Algorithm that solves the SAT problem by brute force.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class BruteForceSatAlgorithm implements Algorithm {

    private SatInput input;

    public BruteForceSatAlgorithm(SatInput input) {
        this.input = input;
    }

    /**
     * {@inheritDoc}
     */
    public Output solve() {
        long startTime = System.currentTimeMillis();
        SatPopulationCnf population = new SatPopulationCnf(input);

        int vectorSize = 0;
        for (ClauseCNF clause : input.getClauses()) {
            vectorSize += clause.getSize();
        }
        int treeDepth = vectorSize + 1;

        boolean[] solution = null;
        int actualBestResult = -1;

        long treeArraySize = ((long) Math.pow(2, treeDepth)) - 1;
        long leavesStartIdx = (treeArraySize / 2); // leaves are second half of array
        for (long i = leavesStartIdx; i < treeArraySize; i++) {
            boolean[] variant = reconstructVariant(i, treeDepth);
            if (population.satisfies(new PopulationMember(variant))) {
                int result = getResult(variant, input.getWeights());
                if (result > actualBestResult) {
                    actualBestResult = result;
                    solution = variant;
                }
            }
        }

        int fitness = population.getFitness(new PopulationMember(solution));
        long elapsed = System.currentTimeMillis() - startTime;
        return new Output(solution, fitness, elapsed);
    }

    /**
     * Reconstructs a variant.
     *
     * @param startIdx  starting index for reconstruction
     * @param treeDepth depth of the tree
     * @return reconstructed variant
     */
    private static boolean[] reconstructVariant(long startIdx, int treeDepth) {
        int variantSize = treeDepth - 1;
        boolean[] variant = new boolean[variantSize];
        long idx = startIdx;
        for (int i = variantSize - 1; i >= 0; i--) {
            variant[i] = (idx % 2 == 0);
            idx = ((idx + 1) / 2) - 1;
        }
        return variant;
    }

    /**
     * Gets the result from variant.
     *
     * @param variant variant for result
     * @param weights weights
     * @return result for variant
     */
    private static int getResult(boolean[] variant, int[] weights) {
        int weightsSum = 0;
        for (int i = 0; i < variant.length; i++) {
            int mul = (variant[i]) ? 1 : 0;
            weightsSum += mul * weights[i];
        }
        return weightsSum;
    }

}
