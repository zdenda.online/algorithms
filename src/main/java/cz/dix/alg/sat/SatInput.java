package cz.dix.alg.sat;

/**
 * Class representing input of the SAT problem.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class SatInput {

    private final int[] weights;
    private final ClauseCNF[] clauses;

    public SatInput(int[] weights, ClauseCNF[] clauses) {
        this.weights = weights;
        this.clauses = clauses;
    }

    public int[] getWeights() {
        return weights;
    }

    public ClauseCNF[] getClauses() {
        return clauses;
    }
}
