package cz.dix.alg.sat;

import cz.dix.alg.Utils;
import cz.dix.alg.genetics.Population;
import cz.dix.alg.genetics.PopulationMember;

/**
 * Implementation of the population for the SAT problem.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class SatPopulationCnf extends Population {

    private int[] weights;
    private ClauseCNF[] clauses;

    /**
     * Creates a new SAT population.
     *
     * @param input input of SAT problem
     * @param size  size of the population
     */
    public SatPopulationCnf(SatInput input, int size) {
        this.weights = input.getWeights();
        this.clauses = input.getClauses();
        for (int i = 0; i < size; i++) {
            boolean[] parts = Utils.genRandomBoolArray(weights.length);
            PopulationMember member = new PopulationMember(parts);
            members.add(member);
        }
    }

    public SatPopulationCnf(SatInput input) {
        this.weights = input.getWeights();
        this.clauses = input.getClauses();
    }

    /**
     * {@inheritDoc}
     */
    public PopulationMember createMember(boolean[] parts) {
        return new PopulationMember(parts);
    }

    /**
     * {@inheritDoc}
     */
    public Population emptyClone() {
        return new SatPopulationCnf(new SatInput(weights, clauses));
    }

    /**
     * {@inheritDoc}
     */
    public int getFitness(PopulationMember member) {
        boolean[] array = member.getParts();
        int weightsSum = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i]) {
                weightsSum += weights[i];
            }
        }
        weightsSum -= member.getPenalization();
        return weightsSum;
    }

    /**
     * {@inheritDoc}
     */
    public boolean satisfies(PopulationMember member) {
        boolean[] array = member.getParts();
        for (ClauseCNF clause : clauses) {
            if (!clause.isFulfilled(array)) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public double getPercentCompletion(PopulationMember member) {
        int clausesCnt = clauses.length;
        int clausesFulfilled = 0;
        boolean[] array = member.getParts();
        for (ClauseCNF clause : clauses) {
            if (clause.isFulfilled(array)) {
                clausesFulfilled++;
            }
        }
        return (double) clausesFulfilled / clausesCnt;
    }
}
