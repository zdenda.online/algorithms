package cz.dix.alg.sat;

import cz.dix.alg.genetics.GeneticsAlgorithm;
import cz.dix.alg.genetics.Output;
import cz.dix.alg.genetics.strategy.impl.*;

/**
 * Tests the SAT algorithms.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class SatTest {

    public static void main(String[] args) throws Exception {
        int weightLimit = 25;
        int clausesCnt = 50;
        int variablesCnt = 20;
        SatInput input = InstancesGenerator.generateInput(weightLimit, variablesCnt, clausesCnt);

        int runs = 10;
        int populationSize = 300;
        int tournamentSize = 5;
        int generationsCnt = 50;
        double pairingProbability = 0.9;
        double mutationProbability = 0.1;

        double lowestSecs = Double.MAX_VALUE;
        for (int run = 0; run < runs; run++) {

            GeneticsAlgorithm algorithm = new GeneticsAlgorithm.Builder()
                    .pairingProbability(pairingProbability)
                    .mutationProbability(mutationProbability)
                    .selectionStrategy(new TournamentSelectionStrategy(tournamentSize))
                    .partnerSelectionStrategy(new RandomPartnerSelectionStrategy())
                    .pairingStrategy(new OnePointPairingStrategy())
                    .replacementStrategy(new WorstReplacementStrategy())
                    .restrictionStrategy(new PercentPenalizationRestrictionStrategy())
                    .mutationStrategy(new RandomPartMutationStrategy())
                    .endingStrategy(new GenerationsCountEndingStrategy(generationsCnt))
                    .population(new SatPopulationCnf(input, populationSize))
                    .build();

            Output output = algorithm.solve();
            if (output != null) {
                double secs = ((double) output.getElapsedTime()) / 1000;
                if (secs < lowestSecs) {
                    lowestSecs = secs;
                }
                System.out.println(output);
            }
        }

        System.out.println("Lowest time: " + lowestSecs + "s");
    }
}
