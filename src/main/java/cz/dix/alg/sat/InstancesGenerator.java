package cz.dix.alg.sat;

/**
 * Generator of SAT input instances.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class InstancesGenerator {

    public static SatInput generateInput(int weightLimit, int variablesCnt, int clausesCnt) {
        int[] weights = new int[variablesCnt];
        for (int i = 0; i < variablesCnt; i++) {
            weights[i] = ((int) (Math.random() * weightLimit)) + 1;
        }

        ClauseCNF[] clauses = new ClauseCNF[clausesCnt];
        final int CLAUSE_SIZE = 3; // 3SAT
        for (int i = 0; i < clausesCnt; i++) {
            int[] clauseArray = new int[CLAUSE_SIZE];
            int idx = 0;
            while (idx != CLAUSE_SIZE) {
                int rndVar = ((int) (Math.random() * variablesCnt)) + 1;
                rndVar = (Math.random() > 0.5) ? -rndVar : rndVar; // negation 50:50
                if (!containsVariable(clauseArray, rndVar)) {
                    clauseArray[idx++] = rndVar;
                }
            }
            clauses[i] = new ClauseCNF(clauseArray);
        }
        return new SatInput(weights, clauses);
    }

    private static boolean containsVariable(int[] clauseArray, int variable) {
        for (int clause : clauseArray) {
            if (clause == variable || clause == -variable) {
                return true;
            }
        }
        return false;
    }
}
