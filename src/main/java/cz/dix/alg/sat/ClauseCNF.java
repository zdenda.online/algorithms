package cz.dix.alg.sat;

/**
 * Simple CNF clause.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class ClauseCNF {

    private int[] clauseArray;

    public ClauseCNF(int[] clauseArray) {
        this.clauseArray = clauseArray;
    }

    /**
     * Checks whether array fulfills the clause.
     *
     * @param array array to be checked
     * @return true whether clause is fulfilled, otherwise false
     */
    public boolean isFulfilled(boolean[] array) {
        for (int idx : clauseArray) {
            if (idx < 0) { // negated
                if (!array[(-idx - 1)]) {
                    return true;
                }
            } else { // NOT negated
                if (array[(idx - 1)]) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Gets size of the clause.
     *
     * @return size of the clause
     */
    public int getSize() {
        return clauseArray.length;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < clauseArray.length; i++) {
            sb.append(clauseArray[i]);
            if (i != clauseArray.length - 1) {
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
